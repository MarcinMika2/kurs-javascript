// Zadanie 1
var book = {};

book.title = '';
book.author = '';
book.numberOfPages = '';

console.log(book);

// Zadanie 2

var person = {};

person.name='';
person.age='';
person.sayHello=function sayHello () {
    return 'hallo';
}

console.log(person);
console.log(person.sayHello());

// Zadanie 3

var train = {};

train instanceof Object;

// Zadanie 4

var car = {};

car.brand = 'Mercedes';
car.color = 'Czarny';
car.numberOfKilometers = 150;
car.printCarinfo=function () {
    return (this.color +' '+ this.brand + ','+ this.numberOfKilometers);
}
car.drive = function (argument) {
    this.numberOfKilometers = this.numberOfKilometers + argument;
}

// car.printCarinfo(); => "Czarny Mercedes, 150km"
// car.drive(20);
// car.printCarinfo(); => "Czerny mercedes, 170km"

// Zadanie 5

car.datyPrzegladow = ['1 maj 2015','1 maj 2016','1 maj 2017'];
car.addDate=function (argument) {
    this.datyPrzegladow.push(argument);
}
car.pokazPrzeglady=function () {
    this.datyPrzegladow.forEach( function(element, index) {
        console.log(element)
    });
}

// Zadanie 6

var bird = {
    type : '',
    name : '',
    getType : function () {
        return this.type;
    }
}

// bird instanceof Object;
// true

// Zadanie 7

var myString = 'teks';
//myNumber instanceof Number => false
//myNumber instanceof String => false
//myNumber instanceof Object => false
//myString instanceof Boolean => false
var myNumber = 35;
//myNumber instanceof Number => false
//myNumber instanceof String => false
//myNumber instanceof Object => false
//myString instanceof Boolean => false


// Zadanie 8

// zrobione

// Zadanie 9

var Rectangle = {
    width: '3',
    height: '3',
    getArea: function () {
        var pole = this.width*this.height
        return pole;
    },
    getPerimiter: function () {
        var obwod = (2*this.width) + (2*this.height)
        return obwod;
    }
}

nowaFigura1 = Object.create(Rectangle);
nowaFigura2 = Object.create(Rectangle);
nowaFigura3 = Object.create(Rectangle);

//nowaFigura1.hasOwnProperty('getArea')
// 12:08:47.099 false
// 12:08:47.099 nowaFigura1.hasOwnProperty('getPermiter')
// 12:08:47.099 false

// Zadanie 10

var calculator = function () {
    this.operations = [],
    this.printOperations = function () { return this.operations },
    this.clearOperations = function () { this.operations = [] }

}

jeden = new calculator()


calculator.prototype.add = function(num1, num2){
    var result = num1+num2;
    var opis = 'added ' + num1 + ' to '  + num2 + ' got ' + result;
    this.operations.push(opis);
};

calculator.prototype.multiply = function(num1, num2) {
    var result = num1*num2 ;
    var opis = 'multiply ' + num1 + ' to '  + num2 + ' got ' + result;
    this.operations.push(opis);
};

calculator.prototype.subtract = function(num1, num2) {
    var result = num1-num2 ;
    var opis = 'subtract ' + num1 + ' to '  + num2 + ' got ' + result;
    this.operations.push(opis);
};

calculator.prototype.divide = function(num1, num2) {
    var result = num1/num2 ;
    var opis = 'divide ' + num1 + ' to '  + num2 + ' got ' + result;
    this.operations.push(opis);
};

calculator.printOperations = function () {
     console.log(this.operations)
};

calculator.clearoperations = function () {
    this.operations = []
}
