// Zadanie 2

function showMax (arguments) {
    var tablica = arguments;
    arguments.forEach( function(element, index) {
        if (element>tablica[0])
            {tablica[0]=element}
    });
    return tablica[0];
}


// Zadanie 3

function countHello (argument) {
    var licznik = 0;

    var interval = setInterval(function () { hello() },1000);

     function hello () {

        if (licznik < argument){

        console.log('hello');
        licznik ++;
        console.log('licznik : ' + licznik)
        }
        else {
            clearInterval(interval);
        }
    }
}

// Zadanie 4

//mamy doczynienia z hoistingiem czyli funkcja i jej wartosci sa zadeklarowane
//  w pamięci przed wykonaniem kodu dlatego działa wywołanie funkcji przed jej
// deklaracją


function test2 () {

hello();

    function hello () {
    console.log('hello');
    };
}

// inna sytuacja jest ze zeminna, javascript deklaruje zmienna ale nie jej wartosc
//dlatego wywołanie zeminnej przed jej deklaracja w kodzie powoduje w tym przypadku
// błąd w przypadku kiedy by była tam wartość nie funkcja dpstalibysmy undefined

function test (argument) {

witaj();

    var witaj = function () {
    console.log('witaj');
    };
}

