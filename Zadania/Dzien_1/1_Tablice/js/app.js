// Zadanie 1 z wykładowcą
function distFromAvarage (argument) {

    var sum = 0;
    var tablica = [];

    argument.forEach( function(element, index) {
        sum = sum+element;
    });

    var avarage = sum/argument.length;

    argument.forEach( function(element, index) {
        tablica.push(Math.abs( element - avarage ) );
    });

    return tablica
}

// Zadanie 1

var tablicaZadanie1 = ["Banan","Pomarańcz","Jabłko","Winogron"]

console.log(tablicaZadanie1[0]); // Banan

console.log(tablicaZadanie1[tablicaZadanie1.length - 1]); // Winogron

tablicaZadanie1.forEach( function(element, index) {
console.log(element);
});

// Zadanie 2

function createArray(number) {
    var newArray = [];

    for(var counter = 1; counter <= number; counter++) {
        newArray.push(counter);
    }
    return newArray
}

console.log("tablica z liczbami do 6 = " + createArray(6) );
console.log("tablica z liczbami do 1 = " + createArray(1));
console.log("Test dla liczby ujemnej (powinna być pusta tablica) " + createArray(-6));
console.log("Test dla zera (powinna być pusta tablica) " + createArray(0));


// Zadanie 3

function printTable (argument) {
    for (var i=0; i<argument.length; i++){
        console.log(argument[i])
    }
}

// Zadanie 4

function multiply (argument) {
    var wynik = argument[0];
    for (var i=1; i<argument.length;i++){
        wynik = wynik*argument[i]
    }
    return wynik;
}

// Zadanie 5

function getEvenAvarage (argument) {
    var sum = 0;
    var liczParz=0;
    argument.forEach( function(element, index) {
        if (element%2==0){
            sum = sum+element;
            liczParz++;
        }
    });

    var wynik = sum/liczParz;

        if (liczParz==0)
            wynik = null;

        console.log(wynik)
}

// Zadanie 6

function sortArray (argument) {
    argument.sort(function(a, b){return a-b});
    console.log(argument)
}


// Zadanie 7

function addArrays (argument1, argument2) {

    var wynik = [];

if (argument1.length > argument2.length){

    argument1.forEach( function(element, index) {
        if (index <= argument2.length-1){
        var sum = element + argument2[index]
        wynik.push(sum);
        }
        if (index>argument2.length-1)
        wynik.push(element)

    });
}

if (argument1.length < argument2.length){

    argument2.forEach( function(element, index) {

        if (index <= argument1.length-1){
        var sum = element + argument1[index]
        wynik.push(sum);
        }

        if (index>argument1.length-1)
            wynik.push(element);

    });
}

if (argument1.length === argument2.length)

argument2.forEach( function(element, index) {

        var sum = element + argument1[index]
        wynik.push(sum);

    });

    return wynik
}
