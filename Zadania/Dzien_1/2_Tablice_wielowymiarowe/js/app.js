var task1Array = [
    [ 2, 3 ],
    [ "Ala", "Ola" ],
    [ true, false ],
    [ 5, 6, 7, 8 ],
    [ 12, 15, 67 ]
];

var task2Array = [
      [ 1, 2, 3, 4 ],
      [ 5, 6, 7, 8 ],
      [ 9, 10, 11, 12 ]
];

// Zadanie 1

// punk 1
console.log(task1Array[3][2]);

// punkt 2
console.log(task1Array[1].length);

// punk 3

console.log(task1Array[4][2]);

// Zadanie 2

// punkt 1
    task2Array.forEach( function(element, index) {
        console.log(element)
    });

// punkt 2

    task2Array.forEach( function(element, index) {
        console.log(element.length)
    });

// punkt 3

    task2Array.forEach( function(element, index) {

        for (i=0; i<element.length; i++){
            console.log(task2Array[index][i])
        }
    });

// Zadanie 3

function print2DArray (element1) {

    element1.forEach( function(element, index) {

        for (i=0; i<element.length; i++){
            console.log(element1[index][i])
        }
    });
};


// Zadanie 4

var tablicaZadanie4 = [
                        [1,2,3,4],
                        [5,6,7,8],
                        [12,13,15,16]
                        ]


// Zadanie 5

function create2DArray (argument1,argument2) {

    var licz=1
    var tablicaZadanie5 = new Array(argument1)

    for(i=0; i<=argument1-1;i++){

     tablicaZadanie5[i]=[]


        for(var inn = 0;inn <= argument2-1; inn++){

            tablicaZadanie5[i].push(licz);
            licz++;
            console.log(tablicaZadanie5[i][inn])
        }

    }

    return tablicaZadanie5;
}
